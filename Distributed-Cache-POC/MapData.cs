﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POC
{
    [Serializable]
    public class MapData
    {
        public int PlayerID { get; set; }
        public override string ToString()
        {
            return "Player ID:" + PlayerID;
        }
    }
}
