﻿using SharpGaming.DistributedCache;
using System;
using System.Diagnostics;
using System.Linq;

namespace POC
{
    class Program
    {
        private static IDistributedCache<MapData> cache;

        private enum CacheType { Local, Hazelcast, Ignite };

        private static CacheType cacheType;

        private static readonly string cacheName = "poc-map";

        private static int nearCacheSize = 500;

        static void Main(string[] args)
        {
            int userInput = 0;
            SetCache(CacheType.Local);
            do
            {
                userInput = DisplayMenu();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                switch (userInput)
                {
                    case 1:
                        int size = cache.Count;
                        for (int i = size; i < size + 100; i++)
                        {
                            cache.Put(i.ToString(), new MapData { PlayerID = i });
                        }
                        sw.Stop();
                        Console.WriteLine("Data added");
                        break;

                    case 2:
                        sw.Stop();
                        Console.WriteLine("Contents : " + string.Join(", ", cache.Keys));
                        break;

                    case 3:
                        Console.WriteLine("Enter key");
                        string key = Console.ReadLine();
                        sw.Restart();
                        MapData data = cache.Get(key);
                        sw.Stop();
                        if (data != null)
                        {
                            Console.WriteLine("Map data = " + data.ToString());
                        }
                        else
                        {
                            Console.WriteLine("Map data not found");
                        }
                        break;

                    case 4:
                        Console.WriteLine("Enter key");
                        string keyToRemove = Console.ReadLine();
                        sw.Restart();
                        cache.Remove(keyToRemove);
                        sw.Stop();
                        Console.WriteLine("Data for " + keyToRemove + " removed");
                        break;

                    case 5:
                        cache.Clear();
                        sw.Stop();
                        Console.WriteLine("All data removed");
                        break;

                    case 6:
                        Console.WriteLine("Number of items in map=" +cache.Count);
                        sw.Stop();
                        break;

                    case 7:
                        for (int i = 0; i < cache.Count; i++)
                        {
                            Console.WriteLine("Retrieval time = " + sw.ElapsedMilliseconds + "ms Map data = " + cache.Get(i.ToString()));
                            sw.Restart();

                        }
                        break;

                    case 8:
                        Console.WriteLine("Enter 1,2 or 3 for Local, Hazelcast or Ignite cache, current cache is " + cacheType.ToString());
                        string type = Console.ReadLine();
                        switch(type)
                        {
                            case "1":
                                SetCache(CacheType.Local);
                                break;

                            case "2":
                                SetCache(CacheType.Hazelcast);
                                break;

                            case "3":
                                SetCache(CacheType.Ignite);
                                break;
                        }

                        break;

                }

                Console.WriteLine("Elapsed time = " + sw.ElapsedMilliseconds + "ms");
            } while (userInput != 9);
        }

        private static void SetCache(CacheType type)
        {
            cacheType = type;
            switch (cacheType)
            {
                default:
                    cache = new LocalMemoryCache<MapData>();
                    return;

                case CacheType.Hazelcast:
                    cache = new HazelcastCache<MapData>(cacheName, true, nearCacheSize);
                    return;

                case CacheType.Ignite:
                    cache = new IgniteCache<MapData>(cacheName, true, nearCacheSize);
                    return;
            }
        }

        private static int DisplayMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Distributed Cache POC - Current Cache is " + cacheType.ToString());
            Console.WriteLine();
            Console.WriteLine("1. Add 100 items to the distributed map");
            Console.WriteLine("2. List keys in the map");
            Console.WriteLine("3. Retrieve map contents by key");
            Console.WriteLine("4. Delete an item");
            Console.WriteLine("5. Clear map");
            Console.WriteLine("6. List number of items in the map");
            Console.WriteLine("7. Retrieve all items in the map");
            Console.WriteLine("8. Switch cache");

            Console.WriteLine("9. Exit");
            var result = Console.ReadLine();
            return Convert.ToInt32(result);
        }
    }
}
