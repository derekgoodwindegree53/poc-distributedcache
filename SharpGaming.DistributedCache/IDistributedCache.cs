﻿using System.Collections.Generic;

namespace SharpGaming.DistributedCache
{
    public interface IDistributedCache<T> where T : class
    {
        void Put(string key, T value);

        T Get(string key);
        
        bool Exists(string key);

        bool Remove(string key);

        void Clear();

        T this[string key] { get; set; }

        int Count { get; }

        IEnumerable<string> Keys { get; }

    }
}
