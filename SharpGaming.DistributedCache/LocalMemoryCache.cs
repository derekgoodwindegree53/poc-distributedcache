﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpGaming.DistributedCache
{
    public class LocalMemoryCache<T> : IDistributedCache<T> where T : class
    {
        private readonly Dictionary<string, T> cache = new Dictionary<string, T>();

        public T this[string key]
        {
            get
            {
                return Get(key);
            }
            set
            {
                cache[key] = value;
            }
        }

        public int Count { get { return cache.Count; } }

        public IEnumerable<string> Keys { get { return cache.Keys; } }

        public void Clear()
        {
           cache.Clear();
        }

        public bool Exists(string key)
        {
            return cache.Keys.Contains(key);
        }

        public T Get(string key)
        {
            return Exists(key) ? cache[key] : null;
        }

        public void Put(string key, T value)
        {
            cache[key] = value;
        }

        public bool Remove(string key)
        {
            return cache.Remove(key);
        }
    }
}
