﻿using Hazelcast.Client;
using Hazelcast.Config;
using Hazelcast.Core;
using System;
using System.Collections.Generic;

namespace SharpGaming.DistributedCache
{
    public class HazelcastCache<T> : IDistributedCache<T> where T : class
    {
        private readonly IHazelcastInstance hazelcastClient;

        private readonly IMap<string, T> cache;

        public HazelcastCache(string name, bool useNearCache, int nearCacheSize)
        {
            Environment.SetEnvironmentVariable("hazelcast.logging.level", "info");
            Environment.SetEnvironmentVariable("hazelcast.logging.type", "console");
            ClientConfig config = XmlClientConfigBuilder.Build("hazelcast.config");
            if (useNearCache)
            {
                NearCacheConfig nearCacheConfig = new NearCacheConfig();
                nearCacheConfig.SetMaxSize(nearCacheSize)
                    .SetInvalidateOnChange(true)
                    .SetEvictionPolicy("Lru")
                    .SetInMemoryFormat(InMemoryFormat.Binary);
                config.AddNearCacheConfig(name, nearCacheConfig);
            }

            hazelcastClient = HazelcastClient.NewHazelcastClient(config);
            cache = hazelcastClient.GetMap<string, T>(name);      
        }

        public T this[string key]
        {
            get
            {
                return cache.Get(key);
            }
            set
            {
                cache.Put(key, value);
            }
        }

        public IEnumerable<string> Keys { get { return cache.KeySet(); } }


        public int Count { get { return cache.Size(); } }

        public void Clear()
        {
            cache.Clear();
        }

        public bool Exists(string key)
        {
            return cache.ContainsKey(key);
        }

        public T Get(string key)
        {
            return cache.Get(key);
        }

        public void Put(string key, T value)
        {
            cache.Put(key, value);
        }

        public bool Remove(string key)
        {
            return (cache.Remove(key) != null);
        }
    }
}
