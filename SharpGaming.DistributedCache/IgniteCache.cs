﻿using Apache.Ignite.Core;
using Apache.Ignite.Core.Cache;
using Apache.Ignite.Core.Cache.Configuration;
using Apache.Ignite.Core.Cache.Eviction;
using System.Collections.Generic;

namespace SharpGaming.DistributedCache
{
    public class IgniteCache<T> : IDistributedCache<T> where T : class
    {
        private readonly IIgnite ignite;

        private readonly ICache<string, T> cache;

        public IgniteCache(string name, bool useNearCache, int nearCacheSize)
        {
            Ignition.ClientMode = true;
            ignite = Ignition.StartFromApplicationConfiguration();
            if (useNearCache)
            {
                NearCacheConfiguration nearCacheCfg = new NearCacheConfiguration
                {
                    EvictionPolicy = new LruEvictionPolicy
                    {
                        MaxSize = nearCacheSize
                    }
                };

                cache = ignite.GetOrCreateCache<string, T>(new CacheConfiguration(name), nearCacheCfg);
            }
            else
            {
                cache =  ignite.GetOrCreateCache<string, T>(new CacheConfiguration(name));
            }
        }

        public IEnumerable<string> Keys
        {
            get
            {
                List<string> keys = new List<string>();
                foreach (ICacheEntry<string, T> iter in cache)
                {
                    keys.Add(iter.Key);
                }

                return keys;
            }
        }

        public int Count { get { return cache.GetSize(); } }

        public T this[string key]
        {
            get
            {
                return cache.Get(key);
            }
            set
            {
                cache.Put(key, value);
            }
        }

        public void Clear()
        {
            cache.Clear();
        }

        public bool Exists(string key)
        {
            return cache.ContainsKey(key);
        }

        public T Get(string key)
        {
            return Exists(key) ? cache.Get(key) : null;
        }

        public void Put(string key, T value)
        {
            cache.Put(key, value);
        }

        public bool Remove(string key)
        {
            return cache.Remove(key);
        }
    }
}